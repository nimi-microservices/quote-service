package com.nimi.quotes.clients;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.nimi.quotes.dto.Rating;

import reactor.core.publisher.Flux;

@Service
public class RatingWebClient
{

  @Autowired
  private WebClient.Builder webClientBuilder;

  @HystrixCommand(fallbackMethod = "fallbackGetRatingsByMakePostcodeYearsLicenceHeld",
                  commandProperties = {
                     @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                  }
  )
  public Flux<Rating> getRatingsByMakePostcodeYearsLicenceHeld(String make, String postcode, Integer yearsLicenceHeld)
  {
    WebClient webClient = webClientBuilder.baseUrl("http://rating-service/ratings")
                                          .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.com.nimi.ratings.v1+json")
                                          .defaultHeader(HttpHeaders.USER_AGENT, "quote-service")
                                          .build();
    
    return webClient.get()
                    .uri(uriBuilder -> uriBuilder.path("/rating")
                                                 .queryParam("make", make)
                                                 .queryParam("postcode", postcode)
                                                 .queryParam("yearsLicenceHeld", yearsLicenceHeld)
                                                 .build())
                    .retrieve()
                    .bodyToFlux(Rating.class);
  }
  
  public Flux<Rating> fallbackGetRatingsByMakePostcodeYearsLicenceHeld(String make, String postcode, Integer yearsLicenceHeld)
  {
    return Flux.just(new Rating(new BigDecimal(1000.00), new BigDecimal(100.00)));
  }

}
